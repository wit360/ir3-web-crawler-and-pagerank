"""
OVERVIEW
CS4300 Programming Assignment 3
Wit Tatiyanupanwong (NetID wt255)

ACKNOWLEDGEMENT
The program includes Martin Porter's Porter Stemming algorithm
implemented in Python by Vivake Gupta (v@nano.com)
The source code is freely available at from http://tartarus.org/~martin/PorterStemmer/
"""

import re
import sys
import urllib

from HTMLParser import HTMLParser
from urlparse import urljoin
from urlparse import urlparse
from numpy import *
from porter_stemmer import PorterStemmer

ONLINE   = None
URLFILE  = "test3.txt"
METAFILE = "metadata"

def main():
    if len(sys.argv) != 2 or (sys.argv[1] != "online" and sys.argv[1] != "offline"):
        print "Please specify mode:"
        print "  'python search_engine.py online'  for online mode"
        print "  'python search_engine.py offline' for offline mode"
        sys.exit()
    global ONLINE 
    ONLINE = sys.argv[1] == "online"
    
    searchEngine = SearchEngine(URLFILE) # initialize search engine 
    searchEngine.writeMetadata(METAFILE) # write metadata
 
    while True:
        term = raw_input("Please enter a search term: ").strip()
        if term == "ZZZ": break
        elif term == "":  continue
        else: searchEngine.search(term)
    print "Program terminated."


# Search engine module
class SearchEngine:
    def __init__(self, urls):
        # populate given url list
        self.__webpages = {}
        self.__weburls = {}
        self.__N = 0
        
        with open(URLFILE,'rU') as file:
            for line in file.readlines():
                [id,url] = line.split(',')
                if len(url) < 3: continue
                url = self.normalizeUrl(url)                    # extract URL
                if self.isIndexed(url): continue                # skip duplicate document
                page_id = int(id.strip())                       # extract page-id
                self.__webpages[page_id] = WebPage(page_id,url) # map page-id to WebPage object
                self.__weburls[url] = page_id                   # map url     to page-id
        self.__N = max(self.__webpages.keys());
        self.__doIndex()
    
    
    # use index file to search for a given term
    def search(self, term):
        resultDocs = set()
        for id in self.__webpages.keys():
            for link in self.__webpages[id].inLinks:
                if id in resultDocs: break
                for anchorText in link.anchorTexts:
                    anchorTerms = anchorText.split() # tokenize
                    p = PorterStemmer() # stem
                    anchorTerms = [p.stem(token, 0,len(token)-1) for token in anchorTerms]
                    term = p.stem(term,0,len(term)-1)
                    if term in anchorTerms:
                        resultDocs.add(id)
                        break

        # sort and print out results
        resultDocs = sorted(list(resultDocs),key=lambda x: self.__pageRank.getPageRank(x),reverse=True)
        for rank,docID in enumerate(resultDocs,1):
            webpage = self.__webpages[docID]
            print "\n%2d)  docID=%-3d  pageRank=%0.2f  %s" % \
                  (rank,webpage.id,self.__pageRank.getPageRank(webpage.id), webpage.url)
            print '     - %s' % (webpage.title)
            print '     " %s"' % (webpage.snippet)
        if len(resultDocs) == 0: print "\nThe search term did not match any anchor text"
        print
        
    # parse each webpage
    # extract links
    def __doIndex(self):
        # create NxN zero matrix (B[to][from])
        N = self.__N;
        B = zeros((N,N),float)
        
        # parse each web page
        for id in range(1,N+1): #sorted(self.__webpages):
            if id not in self.__webpages: continue
            webpage = self.__webpages[id]
            file = None
            if ONLINE:
                file = urllib.urlopen(webpage.url)
                print id,webpage.url
            else:
                file = open("test3/" + str(id) + ".html","r")
            html = file.read() # read a page from http or file
            file.close()
            
            # parse HTML for title and links
            parser = WebPageParser(html)
            webpage.title = parser.title   
            webpage.snippet = parser.snippet         

            # process links
            for outlink in parser.outlinks:
                anchor_texts = parser.outlinks[outlink]
                # if len(anchor_texts) > 1: print anchor_texts
                dst_url = self.normalizeUrl(urljoin(webpage.url,outlink))
                dst_url = self.mapUrlToIndex( dst_url )
                if not dst_url in self.__weburls: 
                    continue # skips url outside the list
                
                # skip self link
                id_dst = self.__weburls[dst_url]
                id_src = webpage.id
                if id_dst == id_src:
                    #print ">>>> Self   ", id_src, dst_url
                    continue
                
                # update B matrix
                if B[id_dst-1][id_src-1] == 0:
                    B[id_dst-1][id_src-1] = 1
                    link = Link(id_src, id_dst, anchor_texts)
                    self.__webpages[id_src].outLinks.append(link)
                    self.__webpages[id_dst].inLinks.append(link)
    
        # normalized B matrix
        for j in range(N):
            temp_sum = 0
            for i in range(N):
                temp_sum += B[i][j] # sum on column (outlinks)
            if temp_sum != 0:
                for i in range(N):
                    B[i][j] /= float(temp_sum) # normalize by sum
            else:
                for i in range(N):
                    B[i][j] = 1.0/N # normalize by sum

        # calculate PageRank
        self.__pageRank = PageRank(B,0.85)   
                  
    # write out metadata
    # according to the instruction
    def writeMetadata(self, filename):
        with open(filename,"w") as f:
            for id in range(1,self.__N+1):
                if id not in self.__webpages: continue
                words = set()
                for link in self.__webpages[id].inLinks:
                    for anchorText in link.anchorTexts:
                        for token in anchorText.split():
                            token = re.sub(r'[^a-zA-Z0-9]','', token )
                            if token == "": continue
                            words.add(token)
                print >>f,"docID=%-3d  pageRank=%7.4f  inlink=%-3d  anchorText=[%s]" % \
                           (id,self.__pageRank.getPageRank(id),len(self.__webpages[id].inLinks)," ".join(words))
                            
    # return URL in the simplest form possible
    # remove in-page links (#xxx)
    # remove trailing default documents index / index.html / index.htm
    def normalizeUrl(self,url):
        url = urlparse(url).geturl().strip()
        #print "IN ",url       
        index = url.rfind("#")
        if index != -1:
            url = url[:index]
        if len(url) > 5  and url[-5:]    == "index"       and url[:-5]  in self.__weburls: url = url[:-5]
        elif len(url) > 9  and url[-9:]  == "index.htm"   and url[:-10] in self.__weburls: url = url[:-9]
        elif len(url) > 10 and url[-10:] == "index.html"  and url[:-10] in self.__weburls: url = url[:-10]
        #print "OUT",url
        return url
    
    # return the equivalent URL in the index
    # be a little fuzzy for the trailing slash
    # return URL string that matches the one in the index if possible
    def mapUrlToIndex(self,url):
        if url in self.__weburls: return url 
        if url+"/" in self.__weburls: return url+"/"
        if url[-1] == "/" and url[:-1] in self.__weburls: return url[:-1]
        return url

    # check whether the given URL is in the index
    # return True/Falses
    def isIndexed(self,url):
        return self.mapUrlToIndex(url) in self.__weburls
    
    def dumpB(self,file=None):
        self.__pageRank.dumpB(file)
    
    def dumpW(self,file=None):
        self.__pageRank.dumpW(file)


# PageRank calculation module
class PageRank:
    # initilaize with transition matrix and damping factor
    def __init__(self, B, d):
        #self.__B = B
        #self.__d = d
        N = shape(B)[0]  # useful matrix dimension
        
        R = ones((N,N),float)/N  # for google matrix calculation
        G = d*B + (1-d)*R        # google matrix

        Wk      = matrix(  ones((N,1),float) )       # Wk
        Wk_prev = matrix( zeros((N,1),float) )       # Wk-1
        e       = matrix(  ones((N,1),float) )/10000 # convergence threshold
        
        # repeat until Wk and Wk-1 converges within |e|
        while (Wk - Wk_prev < -e).any() or (Wk - Wk_prev > e ).any():
            Wk_prev = Wk
            Wk = G*Wk_prev
        self.W = Wk          # save final result

    # return PageRank for a given doc ID 
    def getPageRank(self, id):
        return self.W[id-1]

    # for dump B/W for debugging
    """
    def dumpB(self,file=None):
        N = len(self.__B)
        if file != None:
            with open(file,"w") as f:
                print >>f, self.__B
        else:
            print self.__B
    """
    def dumpW(self,file=None):
        N = len(self.__W)
        if file != None:
            with open(file,"w") as f:
                print >>f, self.__W
        else:
            print self.__W


# Parser
# extract links (href) with anchor text, page title and snippet
class WebPageParser(HTMLParser):
    def __init__(self, html):
        HTMLParser.__init__(self)
        self.title = None
        self.snippet = ""
        self.outlinks = {}
        self.__current_href = None
        self.__tag = {"title":False, "a":False, "body":False,"style":False,"script":False}
        self.__snippet_length = 150
        self.feed(html)
        
    def handle_starttag(self, tag, attrs):
        self.__tag[tag] = True
        if tag == "a": 
            for (attr,value) in attrs:
                if attr=="href": 
                    self.__current_href = value
                    if value not in self.outlinks: self.outlinks[value] = []
        
        elif tag == "img" and self.__tag["a"]:
            for (attr,value) in attrs:
                if attr=="alt":
                    self.__addAnchorText(value)
                            
    def handle_endtag(self, tag):
        self.__tag[tag] = False
        if tag == "a":  self.__current_href = None

    def handle_data(self, data):
        if self.__tag["title"]:  self.title = data
        elif self.__tag["a"]:    self.__addAnchorText(data)
        elif self.__tag["body"] and not (self.__tag["style"] or self.__tag["script"]) and len(self.snippet) < self.__snippet_length:
            s = data
            s = re.sub(r'[^a-zA-Z0-9-\. ]','', s )
            s = re.sub(r'\s+',' ', s )
            if len(s) > 1:
                self.snippet = self.snippet + s + " "
                if len(self.snippet) > self.__snippet_length: self.snippet = self.snippet[:self.__snippet_length]

    def __addAnchorText(self, atext):
        if self.__current_href != None and len(atext.strip()) > 2:
            self.outlinks[self.__current_href].append(atext)

        
# Data Structures
# representing a webpage (a node)
class WebPage:
    def __init__(self, id, url):
        self.id  = id                 # as specified in test3.txt
        self.url = url                # as specified in test3.txt
        self.inLinks = []             # list of links having dstID = id
        self.outLinks = []            # list of links having srcID = id
        self.title = None             # title of the web page
        self.snippet = ""             # first few words on the page
    def __str__(self):
        return "%03d %s - %s" % (self.id, self.title, self.url)

# representing a link (an edge)
class Link:
    def __init__(self, src_id, dst_id, anchor_texts):
        self.srcID = src_id             # source webpage id
        self.dstID = dst_id             # destination webpage id
        anchor_texts = [re.sub('\s+',' ', text.strip() ).strip().lower() for text in anchor_texts]
        self.anchorTexts = anchor_texts # anchor text in the linking page
    def __str__(self):
        return "Link from %d to %d (%s)" % (self.srcID, self.dstID, self.anchorText)


if __name__ == '__main__':
    main()
